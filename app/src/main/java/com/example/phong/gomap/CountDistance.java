package com.example.phong.gomap;
// ha ha 
import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class CountDistance extends Activity implements Runnable, View.OnClickListener {
    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 300;

    protected LocationManager locationManager;
    static double n=0;
    Location location;
    Long s1,r1;
    double plat,plon,clat,clon,dis;
    MyCount counter;
    Thread t1;
    EditText e1;
    boolean bool=true;
    String provider;

    Button b1,b2,b3,b4,b5;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_count_distance);
        b1=(Button)findViewById(R.id.button1);//<--- current position
        b2=(Button)findViewById(R.id.button2);//<---- start moving.. calculates distance on clicking this
        b3=(Button)findViewById(R.id.button3);//<--- pause
        b4=(Button)findViewById(R.id.button4);//<-- resume
        b5=(Button)findViewById(R.id.button5);//<-- get distance
                e1=(EditText)findViewById(R.id.editText1);

        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        provider = locationManager.getBestProvider(criteria, false);
        locationManager.requestLocationUpdates(
                provider,MINIMUM_TIME_BETWEEN_UPDATES, MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                new MyLocationListener()   );
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCurrentLocation();
            }
        });

    }

    public double getDistance(double lat1, double lon1, double lat2, double lon2) {
        double latA = Math.toRadians(lat1);
        double lonA = Math.toRadians(lon1);
        double latB = Math.toRadians(lat2);
        double lonB = Math.toRadians(lon2);
        double cosAng = (Math.cos(latA) * Math.cos(latB) * Math.cos(lonB-lonA)) +
                (Math.sin(latA) * Math.sin(latB));
        double ang = Math.acos(cosAng);
        double dist = ang *6371;

        return dist;
    }
    protected void showCurrentLocation() {

        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (location != null) {
            String message = String.format(
                    "Current Location \n Longitude: %1$s \n Latitude: %2$s",
                    location.getLongitude(), location.getLatitude()
            );
            clat=location.getLatitude();
            clon=location.getLongitude();
            Toast.makeText(CountDistance.this, message,
                    Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(CountDistance.this, "null location",
                    Toast.LENGTH_LONG).show();
        }

    }
    public void start (View v){

        switch(v.getId()){

            case R.id.button2:
                t1=new Thread();
                t1.start();
                counter= new MyCount(30000,1000);
                counter.start();
                break;
            case R.id.button3:
                counter.cancel();
                bool=false;
                break;
            case R.id.button4:
                counter= new MyCount(s1,1000);
                counter.start();
                bool=true;
                break;
            case R.id.button5:

                double time=n*30+r1;
                Toast.makeText(
                        CountDistance.this,"distance in metres:"+String.valueOf(dis)+"Velocity in m/sec :"
                                +String.valueOf(dis/time)+"Time :"+String.valueOf(time),Toast.LENGTH_LONG).show();

        }


    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){

            case R.id.button2:
                n = 0;
                t1=new Thread();
                t1.start();
                counter= new MyCount(30000,1000);
                counter.start();
                break;
            case R.id.button3:
                counter.cancel();
                bool=false;
                break;
            case R.id.button4:
                counter= new MyCount(s1,1000);
                counter.start();
                bool=true;
                break;
            case R.id.button5:
                counter.cancel();


                double time=n*30+r1;
                Toast.makeText(
                        CountDistance.this,"distance in metres:"+String.valueOf(dis)+"Velocity in m/sec :"
                                +String.valueOf(dis/time)+"Time :"+String.valueOf(time),Toast.LENGTH_LONG).show();
                dis = 0.0;
                //n = 0;

        }
    }


    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location loca) {
            location = loca;

            String message = String.format(
                    "New Location \n Longitude: %1$s \n Latitude: %2$s",
                    location.getLongitude(), location.getLatitude()
            );

            Toast.makeText(CountDistance.this, message, Toast.LENGTH_LONG).show();
        }
        @Override
        public void onStatusChanged(String s, int i, Bundle b) {
            Toast.makeText(CountDistance.this, "Provider status changed",
                    Toast.LENGTH_LONG).show();
        }
        @Override
        public void onProviderDisabled(String s) {
            Toast.makeText(CountDistance.this,
                    "Provider disabled by the user. GPS turned off",
                    Toast.LENGTH_LONG).show();
        }
        @Override
        public void onProviderEnabled(String s) {
            Toast.makeText(CountDistance.this,
                    "Provider enabled by the user. GPS turned on",
                    Toast.LENGTH_LONG).show();
        }

    }
    public class MyCount extends CountDownTimer {
        public MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }
        @Override
        public void onFinish() {
            counter= new MyCount(30000,1000);
            counter.start();
            n=n+1;
        }
        @Override
        public void onTick(long millisUntilFinished) {

            clat = location.getLatitude();
            clon = location.getLongitude();
            if (clat != plat || clon != plon) {
                dis += getDistance(plat, plon, clat, clon);
                plat = clat;
                plon = clon;

            }
            s1=millisUntilFinished;
            r1=(30000-s1)/1000;
            e1.setText(String.valueOf(r1));


        }
    }
    @Override
    public void run() {
        while (bool) {
            clat = location.getLatitude();
            clon = location.getLongitude();
            if (clat != plat || clon != plon) {
                dis += getDistance(plat, plon, clat, clon);
                plat = clat;
                plon = clon;

            }

        }

    }
}
